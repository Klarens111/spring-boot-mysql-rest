package com.example.mysqlRest.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "phone_types")
@Data
@EqualsAndHashCode(callSuper=true)
public class PhoneType extends AbstractType {
}
