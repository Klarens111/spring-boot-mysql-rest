package com.example.mysqlRest;

import com.example.mysqlRest.dao.CountryDaoImpl;
import com.example.mysqlRest.model.Country;
import com.example.mysqlRest.model.pax.Passenger;
import com.example.mysqlRest.repository.PassengerRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
class PaxAddCountryIT {
    @Autowired
    PassengerRepository passengerRepository;
    @Autowired
    CountryDaoImpl countryDaoImpl;

    @Test
    @Transactional
    @Commit
    void addCountries() {
        Optional<Passenger> optionalPassenger = passengerRepository.findById(50L);

        Passenger pax = optionalPassenger.get();

        pax.getAddresses();
        pax.getPhones();

        List<Country> list = countryDaoImpl.list();
        Country lv = countryDaoImpl.findByCodeFromCache("LV");
        Country ru = countryDaoImpl.findByCode("RU");

        /*pax.getVisitedCountries().add(lv);
        pax.getVisitedCountries().add(ru);
        passengerRepository.save(pax);*/
    }
}